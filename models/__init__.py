# -*- coding: utf-8 -*-

from . import patient
from . import reservation

from . import medical_record

from . import doctor
from . import rating
from . import wallet
from . import transaction

from . import history
from . import hospital

from . import examination_group
from . import examination
from . import reservation_examination
from . import reservation_examination_image

from . import reservation_image
from . import user
from . import authentication
from . import bank_account
from . import contract
