# -*- coding: utf-8 -*-
from openerp import models, fields, api

class FaAuthentication(models.Model):
    _name = 'fa_authentication'

    provider             = fields.Char("provider")
    uid                  = fields.Char("uid")
    user_id              = fields.Many2one('fa_user' ,ondelete='set null', string='用户', index=True)
    unionid              = fields.Char("unionid")
    nickname             = fields.Char("nickname")
    created_at           = fields.Datetime('创建时间')
    updated_at           = fields.Datetime('更新时间')
