# -*- coding: utf-8 -*-
from openerp import models, fields, api

class FaBankAccount(models.Model):
    _name = 'fa_bank_account'

    user_id              = fields.Many2one('fa_user' ,ondelete='set null', string='用户', index=True)
    account_name         = fields.Char("account_name")
    account_number       = fields.Char("account_number")
    bank_name            = fields.Char("bank_name")
    created_at           = fields.Datetime('创建时间')
    updated_at           = fields.Datetime('更新时间')
