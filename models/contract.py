# -*- coding: utf-8 -*-
from openerp import models, fields, api


class FaContract(models.Model):
    _name = 'fa_contract'

    doctor_id            = fields.Many2one('fa_doctor' ,ondelete='set null', string='医生', index=True)
    type                 = fields.Char('type')
    months               = fields.Integer('months')

    # bank_account_id      = fields.One2one('fa_bank_account' ,ondelete='set null', string='银行账户', index=True, required=False)

    start_at             = fields.Datetime('开始时间')
    end_at               = fields.Datetime('结束时间')

    created_at           = fields.Datetime('创建时间')
    updated_at           = fields.Datetime('更新时间')
