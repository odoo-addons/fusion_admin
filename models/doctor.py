# -*- coding: utf-8 -*-
from openerp import models, fields, api


class FaDoctor(models.Model):
    _name = 'fa_doctor'

    name = fields.Char('name', required = True)
    gender = fields.Selection([('1', "男"), ('0', '女')], string='gender')
    age = fields.Integer('age')
    date_of_birth = fields.Datetime('date_of_birth')
    mobile_phone = fields.Char('mobile_phone')
    remark = fields.Char('remark')
    id_card_num = fields.Char('身份证号码')
    id_card_front = fields.Binary('身份证正面')
    id_card_back = fields.Binary('身份证反面')
    id_card_front_media_id = fields.Char('id_card_front_media_id')
    id_card_back_media_id = fields.Char('id_card_back_media_id')
    license_front_media_id = fields.Char('license_front_media_id')
    license_back_media_id = fields.Char('license_back_media_id')
    license_front = fields.Binary('医生证件正面')
    license_back = fields.Binary('医生证件反面')
    job_title = fields.Char('job_title')
    user_id = fields.Many2one('fa_user', ondelete='set null', string='用户',
                              index=True)
    hospital = fields.Char('hospital')
    good_at = fields.Char('good_at')
    avatar = fields.Binary('avatar')

    work_address = fields.Char('work_address')
    home_address = fields.Char('home_address')

    created_at           = fields.Datetime('created_at')
    updated_at           = fields.Datetime('updated_at')

    aasm_state = fields.Selection(selection=[
        ('pending', ' 待审核'), ('verified', '审核通过'), ('failed',  '审核不通过'),
        ('signed',  '已签约')
        ], string='aasm_state', default='pending')

    graduate_institution = fields.Char('graduate institution')
    
    # 新添加关联
    reservations_ids = fields.One2many('fa_reservation', 'doctor_id', string='医生的预约')
    

    # @api.one
    # def verify(self):
    #     self.verified = True
    #     return True
    # 改变审核状态

    # @api.multi
    # def do_confirm(self):
    #     self.state = 'confirm'
    #     self.verified = False
    #     return True

    # @api.multi
    # def do_complete(self):
    #     self.state = 'complete'
    #     self.verified = True
