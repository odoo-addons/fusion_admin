# -*- coding: utf-8 -*-
from openerp import models, fields, api

class FaReservation(models.Model):
    _name = 'fa_reservation'

    aasm_state           = fields.Selection(selection=[
                                             ('to_prepay', ' 待支付定金'), ('prepaid', '匹配中'), ('to_examine',  '待检查'),
                                            ('to_consult', '待咨询'), ('consulting', '咨询中'), ('to_pay', '待支付'), ('paid', '已支付'),
                                            ('diagnosed', '医生服务已完成'),('rated', '已评价'), ('cancelled', '已取消'), ('overdued', '超时未支付定金'),
                                             ], string='state', default='to_prepay')
    # 用户想预约在什么时候
    p_date               = fields.Datetime('用户预约时间')
    # 用户预约的时候所在位置
    p_location           = fields.Char('用户预约位置')
    # reservation_type     = fields.Selection(selection=[('offline', 'offline'), ('online', 'online')], string='reservation_type')
    # 用户预约的时候填的手机号
    p_phone              = fields.Char(string="用户预约手机")

    # 医生确定好的预约时间，地点
    d_date               = fields.Datetime('医生确定的时间')
    d_location           = fields.Char('医生确定的位置')

    out_trade_prepay_no  = fields.Char('out_trade_prepay_no')
    out_trade_pay_no     = fields.Char('out_trade_pay_no')

    prepay_fee           = fields.Integer(string='prepay_fee', default=0)
    pay_fee              = fields.Integer(string='pay_fee', default=0)
    total_fee            = fields.Integer(string='total_fee', default=0)

    chief_complains      = fields.Char('chief_complains')
    reservation_name     = fields.Char('reservation_name')
    reservation_remark   = fields.Char('reservation_remark')
    # required
    user_id              = fields.Many2one('fa_user' ,ondelete='set null', string='预约人',
                                          index=True)
    # doctor_id            = fields.Integer(string="医生")
    assistant_id         = fields.Many2one('fa_user' ,ondelete='set null', string='助手',
                                    index=True)
    family_member_id     = fields.Many2one('fa_user' ,ondelete='set null', string='小孩',
                                    index=True)

    height               = fields.Integer(string="身高")
    weight               = fields.Integer(string="体重")

    can_be_arranged      = fields.Boolean(string="是否接受调配")

    created_at           = fields.Datetime('创建时间')
    updated_at           = fields.Datetime('更新时间')

    # 10.29添加模型关联doctor_id,删除原来的doctor_id:
    doctor_id = fields.Many2one('fa_doctor', ondelete='set null', string='医生',
                                index=True)

    @api.multi
    def action_prepaid(self):
        self.aasm_state = 'prepaid'
        return True

    @api.multi
    def action_examine(self):
        self.aasm_state = 'to_examine'
        return True

    @api.multi
    def action_to_consult(self):
        self.aasm_state = 'to_consult'
        return True

    @api.multi
    def action_consulting(self):
        self.aasm_state = 'consulting'
        return True

    @api.multi
    def action_to_pay(self):
        self.aasm_state = 'to_pay'
        return True

    @api.multi
    def action_paid(self):
        self.aasm_state = 'paid'
        return True

    @api.multi
    def action_diagnosed(self):
        self.aasm_state = 'diagnosed'
        return True

    @api.multi
    def action_cancelled(self):
        self.aasm_state = 'cancelled'
        return True

    @api.multi
    def action_overdued(self):
        self.aasm_state = 'overdued'
        return True

    @api.multi
    def action_rated(self):
        self.aasm_state = 'rated'
        return True




    # active = fields.Boolean('Active?', default=True)

    # @api.one
    # def do_toggle_done(self):
    #   self.is_done = not self.is_done
    #   return True
    #
    # @api.multi
    # def do_clear_done(self):
    #   done_recs = self.search([('is_done', '=', True)])
    #   done_recs.write({'active': False})
    #   return True
