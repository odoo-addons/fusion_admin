# -*- coding: utf-8 -*-
from openerp import models, fields, api

class FaReservationExaminationImage(models.Model):
    _name = 'fa_reservation_examination_image'

    reservation_examination_id = fields.Integer('reservation_examination_id', required = True)
    data = fields.Char('data', required = False)
    media_id = fields.Char('media_id', required = True)
