# -*- coding: utf-8 -*-
from openerp import models, fields, api

class Transaction(models.Model):
    _name = 'fa_transaction'
    reservation_id = fields.Many2one('reservation_id')
    amount = fields.Float('amount', default=0.0)
    source = fields.Char('source')
    withdraw_target = fields.Char('withdraw_target')
    operation = fields.Char('operation')
    user_id = fields.Many2one('user_id')
    aasm_state    = fields.Selection(selection=[
                                        ('pending', '待定'), ('settled', '已完成')
                                        ], string='state', default='pending')
    created_at           = fields.Datetime('created_at')
    updated_at           = fields.Datetime('updated_at')
