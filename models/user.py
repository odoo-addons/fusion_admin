#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
@Time    : 17-11-1 上午12:39
@Author  : TangZongYu
@Desc    : 
"""
from openerp import models, fields, api


class FaUser(models.Model):
    _name = 'fa_user'
    name = fields.Char('name', required=True)
    email = fields.Char('邮箱')
    encrypted_password = fields.Char('encrypted_password')
    reset_password_token = fields.Char('reset_password_token')
    reset_password_sent_at = fields.Datetime('reset_password_sent_at')
    remember_created_at= fields.Datetime('remember_created_at')
    sign_in_count = fields.Integer('sign_in_count')
    current_sign_in_at = fields.Datetime('current_sign_in_at')
    last_sign_in_at = fields.Datetime('last_sign_in_at')
    current_sign_in_ip = fields.Integer('current_sign_in_ip')
    last_sign_in_ip = fields.Integer('last_sign_in_ip')
    created_at = fields.Datetime('created_at')
    updated_at = fields.Datetime('updated_at')
    gender = fields.Selection([('1', "男"), ('0', '女')], string='gender')
    avatar = fields.Binary('avatar')
    mobile_phone = fields.Char('mobile_phone')
    location = fields.Char('location')
    qrcode=fields.Char('qrcode')
    ancestry=fields.Char('ancestry')
    identity_card=fields.Char('identity')
    birthdate=fields.Datetime('birthdate')
    blood_type=fields.Char('blood_type')
    history_of_present_illness = fields.Text('history_of_present_illness')
    past_medical_history=fields.Text('past_medical_history')
    allergic_history=fields.Text('allergic_history')
    personal_history=fields.Text('personal_history')
    family_history=fields.Text('family_history')
    vaccination_history=fields.Text('vaccination_history')

    # 新添加关联
    reservations_ids = fields.One2many('fa_reservation', 'user_id', string='用户的预约')