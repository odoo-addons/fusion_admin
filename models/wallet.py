# -*- coding: utf-8 -*-
from openerp import models, fields, api

class FaWallet(models.Model):
    _name = 'fa_wallet'

    user_id = fields.Many2one('user_id')
    balance_withdrawable = fields.Float('balance_withdrawable', default=0.0)
    balance_unwithdrawable = fields.Float('balance_unwithdrawable', default=0.0)
    balance_withdrawable = fields.Float('balance_withdrawable', default=0.0)
    created_at           = fields.Datetime('created_at')
    updated_at           = fields.Datetime('updated_at')

